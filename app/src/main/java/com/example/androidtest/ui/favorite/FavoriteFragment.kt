package com.example.androidtest.ui.favorite

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.ui.ArticleAdapter
import com.example.androidtest.database.ArticleDatabase
import com.example.androidtest.model.Article
import com.example.androidtest.repository.ArticleRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class FavoriteFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView

    private lateinit var favoriteViewModel: FavoriteViewModel

    private var favoriteList: List<Article>? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    private fun onClickDeleteFavorite(id: Long) {
        favoriteViewModel.deleteById(id)
    }

    private fun onSelectedArticle(selectedArticle: Article) {
        onClickDeleteFavorite(selectedArticle.id)
    }


    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(itemView, savedInstanceState)
        recyclerView = itemView.findViewById(R.id.recycler_view_favorite)
        val database by lazy {
            ArticleDatabase.getInstance(
                CoroutineScope(SupervisorJob()),
                itemView.context
            )
        }
        val repository by lazy { ArticleRepository(database.articleDao()) }
        favoriteViewModel = FavoriteViewModel(repository)
        favoriteViewModel.getFavoriteArticles.observe(viewLifecycleOwner) { articles ->
            articles.let {
                favoriteList = articles
                recyclerView.apply {
                    layoutManager = LinearLayoutManager(activity)
                    adapter = ArticleAdapter(it, ::onSelectedArticle)
                }
            }
        }
    }
}