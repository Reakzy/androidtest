package com.example.androidtest.ui.favorite

import androidx.lifecycle.*
import com.example.androidtest.model.Article
import com.example.androidtest.repository.ArticleRepository
import kotlinx.coroutines.launch

class FavoriteViewModel(private val repository: ArticleRepository) : ViewModel() {

    val getFavoriteArticles: LiveData<List<Article>> = repository.getFavoriteArticles.asLiveData()

    fun deleteById(id: Long) = viewModelScope.launch {
        repository.deleteById(id)
    }
}