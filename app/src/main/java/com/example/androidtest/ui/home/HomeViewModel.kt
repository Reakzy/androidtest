package com.example.androidtest.ui.home

import androidx.lifecycle.*
import com.example.androidtest.model.Article
import com.example.androidtest.repository.ArticleRepository
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: ArticleRepository) : ViewModel() {

    val getFavoriteArticles: LiveData<List<Article>> = repository.getFavoriteArticles.asLiveData()

    fun deleteById(id: Long) = viewModelScope.launch {
        repository.deleteById(id)
    }

    fun insertOrUpdate(articleToAdd: Article, favoriteList: List<Article>?) =
        viewModelScope.launch {
            repository.insertOrUpdate(articleToAdd, favoriteList)
        }

    fun checkFavoriteList(
        jsonList: MutableList<Article>,
        favoriteList: List<Article>
    ): List<Article> {
        jsonList.forEachIndexed { index, article ->
            val checkArticle = favoriteList.find {
                it.title == article.title
            }
            if (checkArticle != null) {
                jsonList[index] = checkArticle
            }
        }
        return jsonList
    }
}
