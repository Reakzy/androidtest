package com.example.androidtest.ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.model.Article
import com.example.androidtest.utils.formatStringToDate
import com.squareup.picasso.Picasso


class ArticleAdapter(
    private val articleList: List<Article>,
    private val onArticleSelected: (article: Article) -> Unit
) : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.article_list, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.articleTitle.text = articleList[i].title
        viewHolder.articlePublishedDate.text = formatStringToDate(articleList[i].published!!)
        viewHolder.articleFavIcon.buttonDrawable =
            setCheckboxDrawable(viewHolder.itemView.context, articleList[i])
        if (!articleList[i].image.isNullOrBlank()) {
            Picasso.get().load(articleList[i].image).error(R.drawable.ic_dashboard_black_24dp).into(viewHolder.articleImage)
        }
    }

    override fun getItemCount(): Int {
        return articleList.size
    }

    private fun setCheckboxDrawable(context: Context, article: Article): Drawable? {
        if (article.favorite) {
            return AppCompatResources.getDrawable(context, R.drawable.ic_baseline_favorite_24)
        }
        return AppCompatResources.getDrawable(context, R.drawable.ic_baseline_favorite_border_24)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var articleTitle: TextView = itemView.findViewById(R.id.article_title)
        var articlePublishedDate: TextView = itemView.findViewById(R.id.article_published_date)
        var articleFavIcon: CheckBox = itemView.findViewById(R.id.article_fav_icon)
        var articleImage: ImageView = itemView.findViewById(R.id.article_image)

        init {

            articleFavIcon.setOnCheckedChangeListener { _, _ ->
                val selectedArticle =
                    articleList.single { it.title == articleTitle.text.toString() }
                onArticleSelected(selectedArticle)
                if (selectedArticle.favorite) {
                    articleFavIcon.buttonDrawable = AppCompatResources.getDrawable(
                        itemView.context,
                        R.drawable.ic_baseline_favorite_24
                    )
                } else {
                    articleFavIcon.buttonDrawable = AppCompatResources.getDrawable(
                        itemView.context,
                        R.drawable.ic_baseline_favorite_border_24
                    )
                }
            }
        }
    }
}