package com.example.androidtest.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.ui.ArticleAdapter
import com.example.androidtest.database.ArticleApplication
import com.example.androidtest.repository.NewsRepository
import com.example.androidtest.model.Article
import kotlinx.coroutines.DelicateCoroutinesApi

@DelicateCoroutinesApi
class HomeFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView

    private lateinit var homeViewModel: HomeViewModel

    private var favoriteList: List<Article>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    private fun onClickAddFavorite(article: Article) {
        homeViewModel.insertOrUpdate(articleToAdd = article, favoriteList)
    }

    private fun onClickDeleteFavorite(id: Long) {
        homeViewModel.deleteById(id)
    }

    private fun onSelectedArticle(selectedArticle: Article) {
        val selectedArticleFavorite = selectedArticle.favorite
        selectedArticle.favorite = !selectedArticle.favorite
        if (selectedArticleFavorite) {
            onClickDeleteFavorite(selectedArticle.id)
        } else {
            onClickAddFavorite(selectedArticle)
        }
    }

    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(itemView, savedInstanceState)
        recyclerView = itemView.findViewById(R.id.recycler_view_home)
        val application = ArticleApplication(itemView.context)
        homeViewModel = HomeViewModel(application.repository)
        var jsonArticleList = NewsRepository().getArticlesFromJson()
        homeViewModel.getFavoriteArticles.observe(viewLifecycleOwner) { articles ->
            jsonArticleList = homeViewModel.checkFavoriteList(jsonArticleList.toMutableList(), articles)
            recyclerView.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = ArticleAdapter(jsonArticleList, ::onSelectedArticle)
            }
        }
    }
}