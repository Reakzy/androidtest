package com.example.androidtest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_database")
data class Article(
    @ColumnInfo(name = "title") var title: String? = null,
    @ColumnInfo(name = "published") var published: String? = null,
    @ColumnInfo(name = "image") var image: String? = null,
    @ColumnInfo(name = "favorite") var favorite: Boolean = false,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(index = true) var id: Long = 0
)