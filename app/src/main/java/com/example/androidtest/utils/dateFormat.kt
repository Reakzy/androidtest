package com.example.androidtest.utils

import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.ZoneId
import java.util.Locale
import java.time.format.FormatStyle

fun formatStringToDate(notFormatedDate: String): String {
    val format = Instant.parse(notFormatedDate)
    val test: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
        .withLocale(Locale.FRANCE)
        .withZone(ZoneId.systemDefault())
    return test.format(format)
}