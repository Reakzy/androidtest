package com.example.androidtest.repository

import androidx.annotation.WorkerThread
import com.example.androidtest.database.ArticleDao
import com.example.androidtest.model.Article
import kotlinx.coroutines.flow.Flow

class ArticleRepository(private val articleDao: ArticleDao) {

    val getFavoriteArticles: Flow<List<Article>> = articleDao.getFavoriteArticles()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(articleToAdd: Article) {
        articleDao.insert(articleToAdd)
    }

    @WorkerThread
    suspend fun update(articleToUpdate: Article) = articleDao.update(articleToUpdate)

    @WorkerThread
    suspend fun deleteAll() = articleDao.deleteAll()

    @WorkerThread
    suspend fun deleteById(id: Long) = articleDao.deleteById(id)

    suspend fun insertOrUpdate(articleToAdd: Article, favoriteList: List<Article>?) {
         val checkArticle = favoriteList?.any { it.title == articleToAdd.title }
        if (checkArticle == false || favoriteList.isNullOrEmpty()) {
            insert(articleToAdd)
        } else {
            update(articleToAdd)
        }
    }
}