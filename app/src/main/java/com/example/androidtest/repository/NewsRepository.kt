package com.example.androidtest.repository

import android.util.Log
import com.example.androidtest.model.Article
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.MalformedURLException
import java.net.URL

class NewsRepository {
    private suspend fun getJSON(): JSONArray? = withContext(Dispatchers.IO) {
        try {
            val urlJson =
                URL("https://gist.githubusercontent.com/julienbanse/34cdfbd1c094b2dddffce2b5d5533d6b/raw/15b5f322838e08bf8a38985b7aa94f6c758d6741/news.json")
            val stringJson = JSONObject(urlJson.readText())
            stringJson.getJSONObject("data").getJSONArray("items")
        } catch (e: MalformedURLException) {
            Log.d("Exception", e.toString())
            null
        }
    }

    // Get image from JSON Object
    private fun getImageArticle(mediaObjectJson: JSONObject): String {
        try {
            val images: JSONArray = mediaObjectJson.getJSONArray("images")
            return images.getJSONObject(0).getJSONObject("original")["url"] as String
        } catch (e: Exception) {
            println("Crash")
        }
        return ""
    }

    @DelicateCoroutinesApi
    fun getArticlesFromJson(): List<Article> {
        val articles: MutableList<Article> = mutableListOf()
        runBlocking {
            val itemsArray = NewsRepository().getJSON()
            itemsArray?.let {
                for (i in 0 until itemsArray.length()) {
                    val articleJSON = itemsArray.getJSONObject(i)
                    articles.add(
                        Article(
                            articleJSON["title"] as String,
                            articleJSON["published"] as String,
                            getImageArticle(articleJSON["medias"] as JSONObject)
                        )
                    )
                }
            }
        }
        return articles
    }
}