package com.example.androidtest.database

import android.app.Application
import android.content.Context
import com.example.androidtest.repository.ArticleRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class ArticleApplication(context: Context) : Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { ArticleDatabase.getInstance(applicationScope, context) }
    val repository by lazy { ArticleRepository(database.articleDao()) }
}
