package com.example.androidtest.database

import androidx.room.*
import com.example.androidtest.model.Article
import kotlinx.coroutines.flow.Flow

@Dao
interface ArticleDao {

    @Query("SELECT * FROM favorite_database WHERE favorite = :articleFavorite ")
    fun getFavoriteArticles(articleFavorite: Boolean = true): Flow<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(article: Article)

    @Update
    suspend fun update(article: Article)

    @Query("DELETE FROM favorite_database")
    suspend fun deleteAll()

    @Query("DELETE FROM favorite_database WHERE id = :id")
    suspend fun deleteById(id: Long)
}